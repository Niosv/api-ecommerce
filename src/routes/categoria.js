import express from "express";
import {
  addCategoria,
  deleteCategoria,
  getAllCategorias,
  getCategoriaById,
  updateCategoria,
} from "../controller/categoria.js";

const router = new express.Router();

router.get("/categoria", getAllCategorias);
router.get("/categoria/:id", getCategoriaById);
router.post("/categoria", addCategoria);
router.put("/categoria/:id", updateCategoria);
router.delete("/categoria/:id", deleteCategoria);

export default router;
