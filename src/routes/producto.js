import express from "express";
import {
  getAllProductos,
  getProductoById,
  addProducto,
  updateProducto,
  deleteProducto,
  reduceStock,
} from "../controller/producto.js";

const router = new express.Router();

router.get("/producto", getAllProductos);
router.get("/producto/:id", getProductoById);
router.post("/producto", addProducto);
router.put("/producto/:id", updateProducto);
router.put("/productoStock/:id", reduceStock);
router.delete("/producto/:id", deleteProducto);

export default router;
