import express from "express";
import {
  addCompra,
  getCompraById,
  getCompraByUser,
  rechazarCompra,
} from "../controller/compra.js";

const router = express.Router();

router.get("/compra/:id", getCompraById);

router.get("/compraUsuario/:id", getCompraByUser);

router.post("/compra", addCompra);

router.put("/cancelarCompra/:id", rechazarCompra);

export default router;
