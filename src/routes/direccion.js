import express from "express";
import {
  addDireccion,
  deleteDireccion,
  getDireccionById,
  getDireccionByUser,
  updateDireccion,
} from "../controller/direccion.js";

const router = express.Router();

router.get("/direccion/:id", getDireccionById);

router.get("/direccionUser/:id", getDireccionByUser);

router.post("/direccion", addDireccion);

router.put("/direccion/:id", updateDireccion);

router.delete("/direccion/:id", deleteDireccion);

export default router;
