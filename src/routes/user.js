import express from "express";
import {
  blockUser,
  getUserById,
  loginUser,
  registerUser,
  updateUser,
} from "../controller/user.js";

const router = express.Router();

router.get("/user/:id", getUserById);

router.post("/register", registerUser);

router.post("/login", loginUser);

router.put("/user/:id", updateUser);

router.put("/blockUser/:id", blockUser);

export default router;
