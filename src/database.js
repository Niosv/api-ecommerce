//Conexión a la base de datos
import pkg from "pg";
const { Pool } = pkg;
import { config } from "dotenv";

config();

const pool = new Pool({
  user: process.env.DB_USER,
  host: process.env.DB_HOST,
  database: process.env.DB_NAME,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT,
});

pool.on("error", (err, client) => {
  console.error("Error inesperado en el cliente PostgreSQL", err);
  process.exit(-1);
});

export default pool;
