import pool from "../database.js";

export const getDireccionById = async (req, res) => {
  try {
    const { id } = req.params;
    const query = "SELECT * FROM direccion WHERE id = $1";
    const result = await pool.query(query, [id]);
    if (result.rowCount === 0) {
      return res
        .status(404)
        .json({ message: "No se encontró la dirección", ok: false });
    }
    res.status(200).json(result.rows);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

export const getDireccionByUser = async (req, res) => {
  try {
    const user = req.params.id;
    const query = "SELECT * FROM dirección WHERE usuario = $1";
    const result = await pool.query(query, [user]);
    if (result.rowCount === 0) {
      return res
        .status(404)
        .json({ message: "Este usuario no tiene direcciones", ok: false });
    }
    res.status(200).json(result.rows);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

export const addDireccion = async (req, res) => {
  try {
    const { region, comuna, calle, numero, descripcion, usuario } = req.body;
    //TODO: Se debe obtener usuario con JWT
    const query =
      "INSERT INTO direccion (region, comuna, calle, numero, descripcion, usuario) VALUES ($1, $2, $3, $4, $5, $6)";
    await pool.query(query, [
      region,
      comuna,
      calle,
      numero,
      descripcion,
      usuario,
    ]);
    res.status(200).json({ message: "Dirección creada", ok: true });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

export const updateDireccion = async (req, res) => {
  try {
    const { id } = req.params;
    const { region, comuna, calle, numero, descripcion } = req.body;
    const query =
      "UPDATE direccion SET region = $1, comuna= $2, calle = $3, numero = $4, descripcion = $5 WHERE id = $6";
    const result = await pool.query(query, [
      region,
      comuna,
      calle,
      numero,
      descripcion,
      id,
    ]);
    if (result.rowCount === 0) {
      return res
        .status(404)
        .json({ message: "No se encontró la dirección", ok: false });
    }
    res.status(200).json({ message: "Dirección actualizada", ok: true });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

export const deleteDireccion = async (req, res) => {
  try {
    const { id } = req.params;
    const query = "DELETE FROM direccion WHERE id = $1";
    const result = await pool.query(query, [id]);
    if (result.rowCount === 0) {
      return res
        .status(404)
        .json({ message: "No se encontró la dirección", ok: false });
    }
    res.status(200).json({ message: "Se eliminó la dirección", ok: true });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};
