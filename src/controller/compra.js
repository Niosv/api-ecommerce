import pool from "../database.js";

export const getCompraById = async (req, res) => {
  try {
    const { id } = req.params;
    const query = "SELECT * FROM compra WHERE id = $1";
    const result = await pool.query(query, [id]);
    if (result.rowCount === 0) {
      return res
        .status(404)
        .json({ message: "No se encontró la compra", ok: false });
    }
    res.status(200).json(result.rows);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

export const getCompraByUser = async (req, res) => {
  try {
    const user = req.params.id;
    const query = "SELECT * FROM compra WHERE usuario = $1";
    const result = await pool.query(query, [user]);
    if (result.rowCount === 0) {
      return res
        .status(404)
        .json({ message: "Este usuario no tiene compras", ok: false });
    }
    res.status(200).json(result.rows);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

export const addCompra = async (req, res) => {
  try {
    const { usuario, direccion, metodo, monto, nro_seguimiento } = req.body;
    //TODO: Se debe obtener usuario con JWT
    const query =
      "INSERT INTO compra (NOW(), usuario, direccion, metodo, monto, nro_seguimiento,estado ) VALUES ($1, $2, $3, $4, $5, $6)";
    await pool.query(query, [
      usuario,
      direccion,
      metodo,
      monto,
      nro_seguimiento, //TODO: generar nro de seguimiento
      2, //Estado = pendiente
    ]);

    //Juntar puntos

    res.status(200).json({ message: "Compra realizada", ok: true });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

export const rechazarCompra = async (req, res) => {
  try {
    const { id } = req.params;
    const query = "UPDATE compra SET estado = 1 WHERE id = $1";
    const result = await pool.query(query, [id]);
    if (result.rowCount === 0) {
      return res
        .status(404)
        .json({ message: "No se encontró la compra", ok: false });
    }
    res.status(200).json({ message: "Se canceló la compra", ok: true });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};
