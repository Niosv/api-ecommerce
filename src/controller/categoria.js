import pool from "../database.js";

export const getAllCategorias = async (req, res) => {
  try {
    const result = await pool.query("SELECT * FROM categoria");
    res.status(200).json(result.rows);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

export const getCategoriaById = async (req, res) => {
  try {
    const id = req.params.id;
    const query = "SELECT * FROM categoria WHERE id = $1";
    const result = await pool.query(query, [id]);
    if (result.rowCount === 0) {
      return res
        .status(404)
        .json({ message: "La categoría no existe", ok: false });
    }
    res.status(200).json(result.rows);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

export const addCategoria = async (req, res) => {
  try {
    const { nombre } = req.body;
    const query = "INSERT INTO categoria (nombre) VALUES ($1)";
    await pool.query(query, [nombre]);
    res.status(200).json({ message: "Se ha agregado una categoria", ok: true });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

export const updateCategoria = async (req, res) => {
  try {
    const { id } = req.params;
    const { nombre } = req.body;
    const query = "UPDATE categoria SET nombre = $1 WHERE id = $2";
    const result = await pool.query(query, [nombre, id]);
    if (result.rowCount === 0) {
      return res
        .status(404)
        .json({ message: "La categoría no existe", ok: false });
    }
    res
      .status(200)
      .json({ message: `Se actualizó la categoría ${nombre}`, ok: true });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

export const deleteCategoria = async (req, res) => {
  try {
    const { id } = req.params;
    const query = "DELETE FROM categoria WHERE id = $1";
    const result = await pool.query(query, [id]);
    if (result.rowCount === 0) {
      return res
        .status(404)
        .json({ message: "La categoría no existe", ok: false });
    }
    res
      .status(200)
      .json({ message: `Se eliminó la categoría ${id}`, ok: true });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};
