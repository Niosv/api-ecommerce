import pool from "../database.js";
import { generateToken } from "./auth.js";
import crypto from "crypto";

export const getUserById = async (req, res) => {
  try {
    const { id } = req.params;
    const query = "SELECT * FROM usuario WHERE id = $1";
    const result = await pool.query(query, [id]);
    if (result.rowCount === 0) {
      return res
        .status(404)
        .json({ message: "No se encontró al usuario", ok: false });
    }
    res.status(200).json(result.rows[0]);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

export const registerUser = async (req, res) => {
  try {
    const { nombre, apellido, correo, password, telefono } = req.body;

    const query =
      "INSERT INTO usuario (nombre, apellido, correo, password, punto, telefono, perfil, estado, tipo) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9) ON CONFLICT (correo) DO NOTHING returning id, tipo, correo, punto";

    //hash password
    const hash = crypto.createHash("sha256");
    hash.update(password);
    const passwordHashed = hash.digest("hex");

    const result = await pool.query(query, [
      nombre,
      apellido,
      correo,
      passwordHashed,
      0,
      telefono,
      1,
      true,
      1,
    ]);

    if (result.rowCount === 0) {
      return res
        .status(400)
        .json({ message: "El correo ya está en uso", ok: false });
    }

    const user = result.rows[0];
    const token = generateToken(user);
    res.status(200).json({
      message: `Se creó el usuario de ${nombre} ${apellido}`,
      ok: true,
      token,
      user,
    });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

export const loginUser = async (req, res) => {
  try {
    const { correo, password } = req.body;
    const query = "SELECT * FROM usuario WHERE correo = $1";
    const result = await pool.query(query, [correo]);
    if (result.rowCount === 0) {
      return res
        .status(404)
        .json({ message: "No existe un usuario con este correo", ok: false });
    }
    const user = result.rows[0];

    //hash password
    const hash = crypto.createHash("sha256");
    hash.update(password);
    const passwordHashed = hash.digest("hex");

    console.log(user.password);
    console.log(passwordHashed);

    if (user.password === passwordHashed) {
      const userPayload = {
        id: user.id,
        id_perfil: user.tipo,
        correo: user.correo,
        puntos: user.punto,
      };
      const token = generateToken(userPayload);
      return res
        .status(200)
        .json({ message: "Login éxitoso", ok: true, token, user: userPayload });
    }
    res.status(401).json({ message: "Contraseña inválida", ok: false });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

export const updateUser = async (req, res) => {
  try {
    const { id } = req.params;
    const { nombre, apellido, correo, telefono } = req.body;

    const resultCorreo = await pool.query(
      "SELECT id FROM usuario WHERE correo = $1 AND id != $2",
      [correo, id]
    );
    console.log(resultCorreo.rows[0].id);

    if (id !== resultCorreo.rows[0].id) {
      return res
        .status(400)
        .json({ message: "Este correo ya está en uso", ok: false });
    }

    const query =
      "UPDATE usuario SET nombre = $1, apellido = $2, correo = $3, telefono = $4 WHERE id = $5";
    const result = await pool.query(query, [
      nombre,
      apellido,
      correo,
      telefono,
      id,
    ]);
    if (result.rowCount === 0) {
      return res
        .status(400)
        .json({ message: "No se pudo actualizar el usuario", ok: false });
    }
    res.status(200).json({ message: "Actualización éxitosa", ok: true });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

export const blockUser = async (req, res) => {
  try {
    const { id } = req.params;
    const query = "UPDATE usuario SET estado = 2 WHERE id = $1";
    const result = await pool.query(query, [id]);
    if (result.rowCount === 0) {
      return res
        .status(404)
        .json({ message: "No se encontró el usuario", ok: false });
    }
    res.status(200).json({ message: "Se bloqueó al usuario", ok: true });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};
