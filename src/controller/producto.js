import pool from "../database.js";

export const getAllProductos = async (req, res) => {
  try {
    const result = await pool.query("SELECT * FROM producto");
    res.status(200).json(result.rows);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

export const getProductoById = async (req, res) => {
  try {
    const { id } = req.params;
    const query = "SELECT * FROM producto WHERE id = $1";
    const result = await pool.query(query, [id]);
    if (result.rowCount === 0) {
      return res
        .status(404)
        .json({ message: "No existe este producto", ok: false });
    }
    res.status(200).json(result.rows);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

export const addProducto = async (req, res) => {
  try {
    const { nombre, precio, categoria, descripcion, marca, stock, estado } =
      req.body;
    const query =
      "INSERT INTO producto (nombre, precio, categoria, descripcion, marca, stock, estado) VALUES ($1, $2, $3, $4, $5, $6, $7)";
    await pool.query(query, [
      nombre,
      precio,
      categoria,
      descripcion,
      marca,
      stock,
      estado,
    ]);
    res
      .status(200)
      .json({ message: `Se ha agregado el producto ${nombre}`, ok: true });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

export const updateProducto = async (req, res) => {
  try {
    const { id } = req.params;
    const { nombre, precio, categoria, descripcion, marca, stock, estado } =
      req.body;
    const query =
      "UPDATE producto SET nombre = $1, precio = $2, categoria = $3, descripcion = $4, marca = $5, stock = $6, estado = $7 WHERE id = $8";
    const result = await pool.query(query, [
      nombre,
      precio,
      categoria,
      descripcion,
      marca,
      stock,
      estado,
      id,
    ]);
    if (result.rowCount === 0) {
      return res
        .status(404)
        .json({ message: "No existe este producto", ok: false });
    }
    res
      .status(200)
      .json({ message: `Se ha actualizado el producto ${nombre}`, ok: true });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

//Resta la cantidad de productos comprados al stock de productos
export const reduceStock = async (req, res) => {
  try {
    const { id } = req.params;
    const { cantidad } = req.body;
    const query =
      "UPDATE producto SET stock = CASE WHEN stock > 0 + $1 THEN stock - $2 ELSE stock END WHERE id = $3";
    const result = await pool.query(query, [cantidad, cantidad, id]);
    if (result.rowCount === 0) {
      return res.status(404).json({
        message: "No se pudo actualizar el stock del producto",
        ok: false,
      });
    }
    res
      .status(200)
      .json({ message: "Se actualizó el stock del producto", ok: true });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

export const deleteProducto = async (req, res) => {
  try {
    const { id } = req.params;
    const query = "DELETE FROM producto WHERE id = $1";
    const result = await pool.query(query, [id]);
    if (result.rowCount === 0) {
      return res
        .status(404)
        .json({ message: "No existe este producto", ok: false });
    }
    res.status(200).json({ message: "Se eliminó el producto", ok: true });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};
