import express from "express";
import cors from "cors";
import morgan from "morgan";
//Importación de routes
import categoriaRoutes from "./routes/categoria.js";
import productoRoutes from "./routes/producto.js";
import compraRoutes from "./routes/compra.js";
import userRoutes from "./routes/user.js";
import direccionRoutes from "./routes/direccion.js";

const app = express();

const PORT = process.env.PORT || 3000;

app.use(express.json());
app.use(cors());
app.use(morgan("dev"));

//Routes
app.use(categoriaRoutes);
app.use(productoRoutes);
app.use(compraRoutes);
app.use(userRoutes);
app.use(direccionRoutes);

app.listen(PORT, () => {
  console.log(`Servidor corriendo en el puerto ${PORT}`);
});
